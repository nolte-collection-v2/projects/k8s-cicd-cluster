terraform {
  backend "kubernetes" {
    secret_suffix    = "awx-infra"
    load_config_file = true
  }
  required_providers {
    awx = {
      source = "nolte/awx"
    }
    pass = {
      source = "camptocamp/pass"
    }
  }
}
