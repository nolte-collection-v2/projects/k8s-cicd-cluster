data "awx_organization" "default" {
  name = "Default"
}



data "pass_password" "ssh_online_private" {
  path = "private/keyfiles/ssh/online/onlineSecond_rsa"
}
data "pass_password" "ssh_online_public" {
  path = "private/keyfiles/ssh/online/onlineSecond_rsa.pub"
}
data "pass_password" "ssh_online_passphrase" {
  path = "private/keyfiles/ssh/online/passphrase"
}

resource "awx_credential_scm" "github" {
  name            = "github"
  organisation_id = data.awx_organization.default.id
  username        = "nolte"
  ssh_key_data    = data.pass_password.ssh_online_private.full
  ssh_key_unlock  = data.pass_password.ssh_online_passphrase.full
}

data "pass_password" "ssh_ansible_private" {
  path = "private/keyfiles/ssh/ansible_rollout/id_ed25519"
}
data "pass_password" "ssh_ansible_public" {
  path = "private/keyfiles/ssh/ansible_rollout/id_ed25519.pub"
}
data "pass_password" "ssh_ansible_passphrase" {
  path = "private/keyfiles/ssh/ansible_rollout/passphrase"
}

resource "awx_credential_machine" "pi_connection" {
  organisation_id     = data.awx_organization.default.id
  name                = "pi-connections"
  username            = "pirate"
  ssh_key_data        = data.pass_password.ssh_ansible_private.full
  ssh_public_key_data = data.pass_password.ssh_ansible_public.full
  ssh_key_unlock      = data.pass_password.ssh_ansible_passphrase.full
}


resource "awx_credential_machine" "commons_connection" {
  organisation_id     = data.awx_organization.default.id
  name                = "commons-connections"
  username            = "nolte"
  ssh_key_data        = data.pass_password.ssh_ansible_private.full
  ssh_public_key_data = data.pass_password.ssh_ansible_public.full
  ssh_key_unlock      = data.pass_password.ssh_ansible_passphrase.full
}

resource "awx_inventory" "default" {
  name            = "managed_services"
  organisation_id = data.awx_organization.default.id
}

resource "awx_inventory_group" "default" {
  name         = "managed_nodes"
  inventory_id = awx_inventory.default.id
}

#----------------------

resource "awx_project" "base_service_config" {
  name                 = "base-service-configuration"
  scm_type             = "git"
  scm_url              = "https://github.com/nolte/ansible_playbook-baseline-online-server"
  scm_branch           = "feature/centos8-v2"
  scm_update_on_launch = true
  organisation_id      = data.awx_organization.default.id
}

resource "awx_job_template" "boostrap_nodes" {
  name           = "boostrap-nodes"
  job_type       = "run"
  inventory_id   = awx_inventory.default.id
  project_id     = awx_project.base_service_config.id
  playbook       = "playbook-bootstrap-node.yml"
  become_enabled = true
}

resource "awx_job_template_credential" "boostrap_nodes" {
  job_template_id = awx_job_template.boostrap_nodes.id
  credential_id   = awx_credential_machine.commons_connection.id
}

resource "awx_project" "k3s" {
  name                 = "k3s"
  scm_type             = "git"
  scm_url              = "https://github.com/nolte/ansible_playbook-baseline-k3s"
  scm_branch           = "feature/controllable-firelld"
  scm_update_on_launch = true
  organisation_id      = data.awx_organization.default.id
}

resource "awx_project" "inventory" {
  name                 = "inventory"
  scm_type             = "git"
  scm_url              = "git@github.com:nolte/ansible-inventories.git"
  scm_branch           = "feature/tower-structure"
  scm_update_on_launch = true
  organisation_id      = data.awx_organization.default.id
  scm_credential_id    = awx_credential_scm.github.id
}

resource "awx_inventory_source" "k3s" {
  name              = "k3s"
  inventory_id      = awx_inventory.default.id
  source_project_id = awx_project.inventory.id
  source_path       = "./local_v2"
}

#----------

resource "awx_job_template" "k3s" {
  name                    = "install-k3s-base"
  job_type                = "run"
  inventory_id            = awx_inventory.default.id
  project_id              = awx_project.k3s.id
  playbook                = "playbook-install-k3s.yaml"
  become_enabled          = true
  ask_limit_on_launch     = true
  ask_inventory_on_launch = true
}

resource "awx_job_template" "k3s_bootstrap" {
  name                    = "install-k3s-bootstrap"
  job_type                = "run"
  inventory_id            = awx_inventory.default.id
  project_id              = awx_project.k3s.id
  playbook                = "playbook-k8s_boostrap_nodes.yaml"
  become_enabled          = true
  ask_limit_on_launch     = true
  ask_inventory_on_launch = true
}

resource "awx_job_template_credential" "k3s_bootstrap" {
  job_template_id = awx_job_template.k3s_bootstrap.id
  credential_id   = awx_credential_machine.commons_connection.id
}

resource "awx_workflow_job_template" "k3s" {
  name                    = "workflow-k3s"
  organisation_id         = data.awx_organization.default.id
  inventory_id            = awx_inventory.default.id
  ask_limit_on_launch     = true
  ask_inventory_on_launch = true
}

resource "random_uuid" "workflow_node_base_uuid" {}

resource "awx_workflow_job_template_node" "default" {
  workflow_job_template_id = awx_workflow_job_template.k3s.id
  unified_job_template_id  = awx_job_template.k3s.id
  inventory_id             = awx_inventory.default.id
  identifier               = random_uuid.workflow_node_base_uuid.result
}

resource "random_uuid" "workflow_k3s_bootstrap" {}

resource "awx_workflow_job_template_node_success" "k3s" {
  workflow_job_template_node_id = awx_workflow_job_template_node.default.id
  unified_job_template_id       = awx_job_template.k3s_bootstrap.id
  inventory_id                  = awx_inventory.default.id
  identifier                    = random_uuid.workflow_k3s_bootstrap.result
}
