terraform {
  backend "kubernetes" {
    secret_suffix    = "cicd-infra"
    load_config_file = true
  }
  required_providers {
    kubectl = {
      source = "gavinbunney/kubectl"
    }
  }
}
