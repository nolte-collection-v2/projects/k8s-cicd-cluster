
variable "ingress_domain" {
  default = "172.21.0.2.sslip.io"
}

resource "kubernetes_namespace" "awx" {
  metadata {
    name = "awx"
  }
}

resource "kubernetes_namespace" "tekton" {
  metadata {
    name = "tekton-cicd"
  }
}

module "awx" {
  source         = "git::https://gitlab.com/nolte-collection-v2/modules/tf-modules/tf-k8s-awx.git//modules/wrapper"
  namespace      = kubernetes_namespace.awx.metadata[0].name
  ingress_domain = var.ingress_domain
}


module "tekton" {
  source    = "git::https://gitlab.com/nolte-collection-v2/modules/tf-modules/tf-k8s-tekton.git//modules/wrapper"
  namespace = kubernetes_namespace.tekton.metadata[0].name
}
