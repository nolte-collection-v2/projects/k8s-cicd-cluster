//+build mage

package main

import (
    "log"
    "context"
    "github.com/magefile/mage/mg"

	// mage:import
	_ "github.com/nolte/plumbing/cmd/kind"
)

type CICDCluster mg.Namespace

// Deploy Harbor Helm Chart to Cluster.
func (CICDCluster) Deploy(ctx context.Context) {
    log.Printf("Hi!")
}
